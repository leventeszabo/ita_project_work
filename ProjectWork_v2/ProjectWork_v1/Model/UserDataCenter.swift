//
//  UserDataCenter.swift
//  ProjectWork_v2
//
//  Created by Levente Szabó on 2018. 12. 01..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class UserDataCenter {
    static let sharedInstance = UserDataCenter()
    private init (){
        
    }
    
    var users : [User] = []
    
    func getData()
    {
            let ref = Database.database().reference()
        
         ref.child("/").observeSingleEvent(of: .value, with: { (snapshot) in
            if let data = snapshot.value as? [[String : Any]]{
                
                for actualDict in data{
                    let newUser = User(dictionary: actualDict)
                    newUser.printState()
                    self.users.append(newUser)
                }
                NotificationCenter.default.post(name: Notification.Name("getUserData"), object: nil)
            }
            }) { (error) in
                print(error.localizedDescription)
         }
    }
    
    func setData(){
        let ref = Database.database().reference()
        
        
    }
}
