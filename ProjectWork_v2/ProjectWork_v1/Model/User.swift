//
//  user.swift
//  ProjectWork_v1
//
//  Created by Levente Szabó on 2018. 10. 14..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit

enum userGender: String{
    case male
    case female
}

class User {
    var firstName: String
    var lastName: String
    var emailAddress: String
    var password: String   //módosítani kell majd
    var yearOfBirth: Int?    //Még nem biztos, hogy ezzel lesz tárolva
    var monthOfBirth: Int?   //Még nem biztos, hogy ezzel lesz tárolva
    var dayOfBirth: Int? //Még nem biztos, hogy ezzel lesz tárolva
    var birtday: Date?   //Még nem biztos, hogy ezzel lesz tárolva
    var mobileNumber: String?
    var genderOfUser: userGender
    var originCountryOfUser:String?
//    var kindOfUser: Status?
    
    
    init(dictionary: [String : Any]) {
        
        let name = dictionary["name"] as! [String : String]
        let newGender = dictionary["gender"] as! String
        
        firstName = name["firstname"]!
        lastName = name["lastname"]!
        genderOfUser = userGender(rawValue: newGender)!
        emailAddress = dictionary["email"] as! String
        password = dictionary["password"] as! String
        //Date = dictionary["date"]
    }
    
    func printState(){
        print("""
                name: \(firstName) \(lastName)
                gender: \(genderOfUser)
                date: \(birtday)
                email: \(emailAddress)
                password: \(password)
                """)
    }
    
//    init(newFirstName: String, newLastName:String, newEmailAddress:String, newBirthday: String) {
//        <#code#>
//    }
    
//    init (newFirstName: String, newLastName: String, newEmailAddress: String, newYearOfBirth: Int, newMonthOfBirth: Int, newDayOfBirth: Int, newMobileNumber: String, newBirthday: Date, newGenderOfUser: String, newStatus: Status.RawValue)
//    {
//        
//        firstName = newFirstName
//        lastName = newLastName
//        emailAddress = newEmailAddress
//        yearOfBirth = newYearOfBirth    //még nem tudom, hogy ezt fogom-e használni
//        monthOfBirth = newMonthOfBirth  //még nem tudom, hogy ezt fogom-e használni
//        dayOfBirth = newDayOfBirth  //még nem tudom, hogy ezt fogom-e használni
//        birtday = newBirthday   //még nem tudom, hogy ezt fogom-e használni
//        mobileNumber = newMobileNumber
//        genderOfUser = newGenderOfUser  //enum esetén ez így megfelelő, ha a rawValue-t szeretném visszatérési értékként?
//        
//        
//    }
    
//    func userRegistration (newUser: self, newSchool: School?, newPartner: Partner?) -> User
//    {
//        //MARK: User conditions
//
//
//        //MARK: new User
//        let newUser = User()
//
//    }
}
