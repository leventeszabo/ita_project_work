//
//  RegistrateUserViewController.swift
//  ProjectWork_v1
//
//  Created by Levente Szabó on 2018. 10. 16..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit

class RegistrateUserViewController: UIViewController, UITextFieldDelegate, UIAlertViewDelegate {
    //MARK: - Outlets
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordAgainTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var userStatusSegmentedControl: UISegmentedControl!
    @IBOutlet weak var userBirthdayDateTimePicker: UIDatePicker!
    @IBOutlet weak var userGenderSegmentedControl: UISegmentedControl!
    @IBOutlet weak var termsOfUseAcceptSwitch: UISwitch!
    
    //MARK: - Actions
    @IBAction func birthdayPicker(_ sender: UIDatePicker) {
        //Ha a beállított dátummal a felhasználó fiatalabb 13 évesnél, akkor a háttér vagy a betűk színe legyen piros és jelenjen meg egy tájékoztató label
        
    }
    @IBAction func firstNameTextField(_ sender: UITextField) {
//        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//            //super.textFieldShouldBeginEditing()
//            firstNameTextField.attributedPlaceholder = NSAttributedString(string: "Write your first name here", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
//            return false
//    }
//        func textFieldDidEndEditing(_ textField: UITextField)
//        {
//                //super.textFieldDidEndEditing()
//                firstNameTextField.backgroundColor = UIColor.clear
//        }
    }
    @IBAction func lastNameTextField(_ sender: UITextField) {
//        func textFieldShouldBeginEditing (_ textField: UITextField) -> Bool
//        {
//            lastNameTextField.attributedPlaceholder = NSAttributedString(string: "Write your last name here", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
//            return true
//        }
//
//        func textFieldDidEndEditing (_ textField: UITextField)
//        {
//            lastNameTextField.backgroundColor = UIColor.clear
//        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //requirementsOfRegistration()
        // Do any additional setup after loading the view.

        //MARK: Disappear keyboard
        firstNameTextField.delegate = self
        lastNameTextField.delegate   = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        passwordAgainTextField.delegate = self
        termsOfUseAcceptSwitch.isOn = false
        
    }
    //törölni kell innen
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        firstNameTextField.backgroundColor = UIColor.red
//        lastNameTextField.backgroundColor = UIColor.red
//        emailTextField.backgroundColor = UIColor.red
//        passwordAgainTextField.backgroundColor = UIColor.red
//        passwordAgainTextField.backgroundColor = UIColor.red
//
//        return true
//    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if(textField == firstNameTextField)
        {
           if (firstNameTextField.text == nil || firstNameTextField.text!.isEmpty)
           {
            firstNameTextField.backgroundColor = UIColor.orange
            firstNameTextField.placeholder = "Write your fist name here "
            }
            else
           {
            firstNameTextField.backgroundColor = UIColor.green
            }
        }
        else if (textField == lastNameTextField)
        {
            if (lastNameTextField.text == nil || lastNameTextField.text!.isEmpty)
            {
                lastNameTextField.backgroundColor = UIColor.orange
                lastNameTextField.placeholder = "Write your last name here"
            }
            else
            {
            lastNameTextField.backgroundColor = UIColor.green
            }
            
        }
        else if (textField == emailTextField)
        {
            
            if (emailTextField.text == nil || emailTextField.text!.isEmpty)
            {
                emailTextField.backgroundColor = UIColor.orange
                emailTextField.placeholder = "Write your email address here"
                //Email format
            }
            else
            {
            emailTextField.backgroundColor = UIColor.green
            }
            
        }
        else if (textField == passwordTextField)
        {
            if (passwordTextField.text!.count >= 7)
            {
                passwordTextField.backgroundColor = UIColor.green
                if (passwordTextField.text!.count >= 7)
                {
                  passwordAgainTextField.isEnabled = true
                }
                else
                {
                    passwordAgainTextField.isEnabled = false
                    //passwordAgainTextField.isEditing = false
                }
                
            }
            else
            {
                passwordTextField.backgroundColor = UIColor.orange
                passwordAgainTextField.isEnabled = false
            }
        }
        else if (textField == passwordAgainTextField)
        {
            if (passwordAgainTextField.text!.count >= 7)
            {
                
                if (passwordTextField.text == passwordAgainTextField.text)
                {
                    passwordAgainTextField.backgroundColor = UIColor.green
                }
                else
                {
                    passwordTextField.backgroundColor = UIColor.orange
                    passwordAgainTextField.backgroundColor = UIColor.orange
                }
            }
            else
            {
                passwordAgainTextField.backgroundColor = UIColor.orange
            }
        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
     //MARK: declare user Array -> temporary
    var userArray: [User] = []
    
    
    //MARK: Actions
    //MARK: Exit/Cancel
    @IBAction func cancelRegistration(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Save registration
    @IBAction func saveRegistration(_ sender: UIButton) {
        //requirementsOfRegistration()
        
        //MARK: registration steps
        let userStatus = Status(rawValue: userStatusSegmentedControl.selectedSegmentIndex)
        let userGender = Gender(rawValue: userGenderSegmentedControl.selectedSegmentIndex)
        var registrationAlert: UIAlertController? = nil
        if (userStatus! == .student)
        {
            //MARK: Add new student
//            var newStudent = Student()
            newStudent.firstName = firstNameTextField.text
            print("First name: \(newStudent.firstName)")
            newStudent.lastName = lastNameTextField.text
            newStudent.emailAddress = emailTextField.text
            newStudent.password = passwordTextField.text
            //Password method
                if (passwordTextField.text == passwordAgainTextField.text)
                {
                    newStudent.password = passwordTextField.text
                }
//                else
//                {
//                    registrationAlert = UIAlertController(title: "Mistake", message: "Check your passwords again.", preferredStyle: UIAlertController.Style.alert)
//                }
            newStudent.kindOfUser = userStatus
            newStudent.genderOfUser = userGender
            //MARK: minimum years
//                let today = NSDate()
//                let day = userBirthdayDateTimePicker.date
            
//                if (Calendar.current.dateComponents([.year], from: day) >= )
//                {
//                    newStudent.birtday = userBirthdayDateTimePicker.date
//                }
//                else
//                {
//
//                }
            //MARK: Add to array -> temporary
            userArray.append(newStudent)
            print("A diák regisztálása megtörtént.")
        }
        else
        {
            //MARK: Add new partner
            var newPartner = Partner()
            newPartner.firstName = firstNameTextField.text
            newPartner.lastName = lastNameTextField.text
            newPartner.emailAddress = emailTextField.text
            if (passwordTextField.text == passwordAgainTextField.text)
            {
                newPartner.password = passwordTextField.text
            }
//            else
//            {
//                registrationAlert = UIAlertController(title: "Mistake", message: "Check your passwords again,", preferredStyle: UIAlertController.Style.alert)
//            }
            newPartner.kindOfUser = userStatus
            newPartner.genderOfUser = userGender
            //MARK: Add to array -> temporary
            userArray.append(newPartner)
            print("A partner regisztrálása megtörtént.")
            //If
            
            //Close the registration view with animation
            //self.viewDidDisappear(true)
        }
        //MARK: Create CSV
        addtoCsv()
    }


    //MARK: Requirements of registration
    func requirementsOfRegistration ()
    {
        //TextFieldDidEndEditing -> Megfelel-e a jelszó a kért paramétereknek? Erős, közepes, gyenge jelszó 
        //TextFieldShouldEndEditing -> Jelszó hosszának megadásához
        if (firstNameTextField.text == "" || lastNameTextField.text == "" || emailTextField.text == "")
        {
        saveButton.isHidden = true

        }
        else
        {
            saveButton.isHidden = false

        }

    }

    
    //MARK: create and add to csv file
    func addtoCsv ()
    {
        //file name and the path
//        let partnerFileName = "partnerUserList_\(NSDate())"
//        let studentFileName = "studentUserList\(NSDate())"
        let userFileName = "RegisteredUserList"
        let documentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true) //Nem a userem gyökérkönyvtárába szeretném létrehozni a filet, hanem egy arra kijelölt mappába
        //Meg kell vizsgálni az eddigi adatokat
        let fileURL = documentDirURL.appendingPathComponent(userFileName).appendingPathExtension(".csv")
        print("File path: \(fileURL.path)")
        
        //Add to new file
            var csvHeader  = "FirstName, LastName, Email, Password, Birthday, Status"
            for users in userArray
            {
                if (users.kindOfUser == Status(rawValue: userStatusSegmentedControl.selectedSegmentIndex))  //Nem biztos, hogy egy file esetén kell ez a feltéltel
                    {
                            let newLine = "\n\(users.firstName!), \(users.lastName!), \(users.emailAddress!), \(users.password!), \("users.birtday"), \(users.kindOfUser!.rawValue)\n"
                            csvHeader.append(contentsOf: newLine)
                    }
                else
                {
                    continue
                }
            }
        //MARK: - CSV export
        do {
            try csvHeader.write(to: fileURL, atomically: true, encoding: .utf8)
            print("Generate file is sucessful.")
        }
        catch
        {
            let errorView = UIAlertController(title: "Error", message: "Can't generate CSV file", preferredStyle: .alert)
            let errorOKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            let errorCancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//            let errorRetryAction = UIAlertAction(title: "Retry", style: .destructive) { (<#UIAlertAction#>) in
//                <#code#>
//            }
            
            errorView.addAction(errorOKAction)
            errorView.addAction(errorCancelAction)
           // errorView.addAction(errorRetryAction)
        }
    }
    
    //MARK: TextFieldReturn     //Ezt át kellene nézni
    func textFieldShouldReturn (_ textField: UITextField) -> Bool
    {
        view.endEditing(true)
        return true
    }
}
