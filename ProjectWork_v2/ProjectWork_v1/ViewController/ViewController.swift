//
//  ViewController.swift
//  ProjectWork_v1
//
//  Created by Levente Szabó on 2018. 10. 14..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIAlertViewDelegate {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var loginAlert: UIAlertController? = nil
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(downloadFinished), name: Notification.Name("getUserData"), object: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        usernameTextField.delegate = self
        passwordTextField.delegate = self
    

    }

//    @IBAction func RegistrateNewUser(_ sender: UIButton) {
////        let newUser = User(newFirstName: <#T##String#>, newLastName: <#T##String#>, newEmailAddress: <#T##String#>, newYearOfBirth: <#T##Int#>, newMonthOfBirth: <#T##Int#>, newDayOfBirth: <#T##Int#>, newMobileNumber: <#T##String#>, newBirthday: <#T##Date#>, newGenderOfUser: <#T##String#>, newStatus: <#T##String#>)
//
//    }
    
    
    @IBAction func loginButton(_ sender: UIButton) {
        if(usernameTextField.text != "" && passwordTextField.text != "")
        {
            print("bejelentkezés")
        }
        else
        {
            loginAlert = UIAlertController(title: "Mistake", message: "Check your username or password.", preferredStyle: UIAlertController.Style.alert)
            loginAlert?.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(loginAlert!, animated: true, completion: nil)
        }
    //Singleton for add data to profile view
    
    }
    
    
    @objc func downloadFinished(){
        print(UserDataCenter.sharedInstance.users)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
}

