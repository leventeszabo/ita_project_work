//
//  ListTableViewController.swift
//  ProjectWork_v1
//
//  Created by Levente Szabó on 2018. 11. 15..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit

class ListTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, PlaceCellDelegate {
    
    
   
    
   //adatok átadása prepare for segue segítségével
    
    func placeData(at cell: TableViewCell, cellForRowAt indexPath: IndexPath) {

        if let editedPlace = placeListTableView.indexPath(for: cell)
        {
            print(places[editedPlace[indexPath.row]].companyName)
        }
    }
    
    
    
    
    
    let places: [Partner] = [Partner(newCompanyName: "Starbucks", newCompanyCity: "Budapest", newCompanyZip: 0000, newCompanyState: "Pest", newCompanyStreet: "Király", newCompanyStreetNumber: 2, newCompanyLat: 47.503453, newCompanyLon: 19.065961),
                             Partner(newCompanyName: "Costa", newCompanyCity: "Budapest", newCompanyZip: 1111, newCompanyState: "Pest", newCompanyStreet: "Eiffel", newCompanyStreetNumber: 1, newCompanyLat: 47.509540, newCompanyLon: 19.057721),
                             Partner(newCompanyName: "Frei Cafe", newCompanyCity: "Budapest", newCompanyZip: 3333, newCompanyState: "Pest", newCompanyStreet: "Könyves Kálmán", newCompanyStreetNumber: 36, newCompanyLat: 47.475378, newCompanyLon: 19.097835)]
    
    
    

    //MARK: - Outlets
    @IBOutlet weak var filterViewContainer: UIView!
    @IBOutlet weak var placeListTableView: UITableView!
    @IBOutlet weak var filterContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var filterBarButton: UIBarButtonItem!
    
    //MARK: - Actions
    @IBAction func filterBarButton(_ sender: Any) {
        
        //MARK: - Appearing FilterView
        if (filterBarButton.tag == 0)
        {
        filterContainerHeight.constant = 250
        view.setNeedsLayout()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            UIView.animate(withDuration: 0.5) {
                self.filterViewContainer.subviews[0].alpha = 1
            }
        }
        filterBarButton.tag += 1
        }
        else if (filterBarButton.tag == 1)   //MARK: - Disappearing FilterView
        {
            filterContainerHeight.constant = 0
            view.setNeedsLayout()
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            }) { (_) in
                UIView.animate(withDuration: 0.5){
                    self.filterViewContainer.subviews[0].alpha = 0
                }
            }
            filterBarButton.tag = 0
        }
                
    }
    

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: - FilterView
        filterContainerHeight.constant = 0
        
        let filter = Bundle.main.loadNibNamed("FilterView", owner: nil, options: nil)! [0] as! FilterView
        filter.alpha = 0
        filterViewContainer.addSubview(filter)
        filterViewContainer.clipsToBounds = true
        
        //MARK: - TableView
        placeListTableView.delegate = self
        placeListTableView.dataSource = self
        let nib = UINib(nibName: "TableViewCell", bundle: nil)
        placeListTableView.register(nib, forCellReuseIdentifier: "placePrevXibCell")
        
        //UITableViewCell osztály a megjelenítendő tartalomhoz
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let filter = filterViewContainer.subviews[0]
        filter.frame = filterViewContainer.bounds
        placeListTableView.separatorColor = UIColor.blue
    }
    //Megnyitáshoz didselectrowat
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.rowHeight = 250
        return places.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let placeCell = placeListTableView.dequeueReusableCell(withIdentifier: "placePrevXibCell", for: indexPath) as! TableViewCell
        placeCell.setPlace(places[indexPath.row])
        placeCell.delegate = self
        return placeCell
    }
    
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 250
//    }
}
