//
//  MapTabBarView.swift
//  ProjectWork_v1
//
//  Created by Levente Szabó on 2018. 10. 23..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit
import MapKit

class MapTabBarView: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let location = locations.last
        {
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span:MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            self.mapView.setRegion(region, animated: true)
        }
    }
    
    
}
