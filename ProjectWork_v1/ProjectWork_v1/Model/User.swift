//
//  user.swift
//  ProjectWork_v1
//
//  Created by Levente Szabó on 2018. 10. 14..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit

class User: NSObject {
    var firstName: String?
    var lastName: String?
    var emailAddress: String?
    var password: String?   //módosítani kell majd
    var yearOfBirth: Int?    //Még nem biztos, hogy ezzel lesz tárolva
    var monthOfBirth: Int?   //Még nem biztos, hogy ezzel lesz tárolva
    var dayOfBirth: Int? //Még nem biztos, hogy ezzel lesz tárolva
    var birtday: Date?   //Még nem biztos, hogy ezzel lesz tárolva
    var mobileNumber: String?
    var genderOfUser: Gender?
    var originCountryOfUser:String?
    var kindOfUser: Status?
    
    
//    init(newFirstName: String, newLastName:String, newEmailAddress:String, newBirthday: String) {
//        <#code#>
//    }
    
//    init (newFirstName: String, newLastName: String, newEmailAddress: String, newYearOfBirth: Int, newMonthOfBirth: Int, newDayOfBirth: Int, newMobileNumber: String, newBirthday: Date, newGenderOfUser: String, newStatus: Status.RawValue)
//    {
//        
//        firstName = newFirstName
//        lastName = newLastName
//        emailAddress = newEmailAddress
//        yearOfBirth = newYearOfBirth    //még nem tudom, hogy ezt fogom-e használni
//        monthOfBirth = newMonthOfBirth  //még nem tudom, hogy ezt fogom-e használni
//        dayOfBirth = newDayOfBirth  //még nem tudom, hogy ezt fogom-e használni
//        birtday = newBirthday   //még nem tudom, hogy ezt fogom-e használni
//        mobileNumber = newMobileNumber
//        genderOfUser = newGenderOfUser  //enum esetén ez így megfelelő, ha a rawValue-t szeretném visszatérési értékként?
//        
//        
//    }
    
//    func userRegistration (newUser: self, newSchool: School?, newPartner: Partner?) -> User
//    {
//        //MARK: User conditions
//
//
//        //MARK: new User
//        let newUser = User()
//
//    }
}
