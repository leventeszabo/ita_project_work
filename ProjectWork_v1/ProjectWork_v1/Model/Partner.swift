//
//  Partner.swift
//  ProjectWork_v1
//
//  Created by Levente Szabó on 2018. 10. 16..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit

class Partner: User {
    var companyName: String?
    var companyCity: String?
    var companyZip: Int?
    var companyState: String?
    var companyStreet: String?
    //utca típusa hiányzik, enummal kéne
    var companyStreetNumber: Int?
    var companyRate: Double?
    var companyLat: Double?
    var companyLon: Double?
   
//    init(newUser: User, newCompanyName: String, newCompanyCity: String, newCompanyZip: Int, newCompanyStreet: String) {
//
//       super.init(newFirstName: <#T##String#>, newLastName: <#T##String#>, newEmailAddress: <#T##String#>, newYearOfBirth: <#T##Int#>, newMonthOfBirth: <#T##Int#>, newDayOfBirth: <#T##Int#>, newMobileNumber: <#T##String#>, newBirthday: <#T##Date#>, newGenderOfUser: <#T##String#>, newStatus: <#T##String#>)
//
//        companyName = newCompanyName
//    }
//
//    func newPartner (newUser: User, newPartner: Partner) -> Partner?
//    {
//
//    }
    
    override init()
    {}
    
    init (newCompanyName: String, newCompanyCity: String, newCompanyZip: Int, newCompanyState: String, newCompanyStreet: String, newCompanyStreetNumber: Int, newCompanyLat: Double, newCompanyLon: Double)   //Rate lehet kell ide
    {
        self.companyName = newCompanyName
        self.companyCity = newCompanyCity
        self.companyZip = newCompanyZip
        self.companyState = newCompanyState
        self.companyStreet = newCompanyStreet
        self.companyStreetNumber = newCompanyStreetNumber
        self.companyLat = newCompanyLat
        self.companyLon = newCompanyLon
    }
    
    
    
}

