//
//  FilterViewController.swift
//  ProjectWork_v1
//
//  Created by Levente Szabó on 2018. 11. 01..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit

class FilterView: UIView {
    //MARK: Outlets
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var distanceSliderOutlet: UISlider!
    
    //MARK: Actions
    @IBAction func distanceSlider(_ sender: UISlider) {
        //distanceLabel.text = String(Double(distanceSliderOutlet.value)).
    }
    
    @IBAction func internetConnectionSwitch(_ sender: UISwitch) {
        //Is the place has internet connection?
    }
    @IBAction func friendsSwitch(_ sender: UISwitch) {
        //Are any of the user's friends in the place
    }
    
    @IBAction func openSwitch(_ sender: UISwitch) {
        //Is coffee open or not
    }
   
    @IBAction func cancelButton(_ sender: UIButton) {
    //Close the filter view without using filter
        
        
    }
    @IBAction func searchButton(_ sender: UIButton) {
        //search using the filters. List the result(s)
    }
    @IBAction func FilterButton(_ sender: UIBarButtonItem) {
        
    }
//        override func viewDidLoad() {
//        super.viewDidLoad()
//            distanceLabel.text = distan
//        // Do any additional setup after loading the view.
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}
