//
//  TableViewCell.swift
//  ProjectWork_v1
//
//  Created by Levente Szabó on 2018. 11. 15..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit

protocol PlaceCellDelegate {
    func placeData(at cell: TableViewCell, cellForRowAt indexPath: IndexPath)
}

class TableViewCell: UITableViewCell {

    var delegate: PlaceCellDelegate?
    
    //MARK: - Outlets
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeAddressLabel: UILabel!
    @IBOutlet weak var placeRateLabel: UILabel!
    @IBOutlet weak var placeDistanceLabel: UILabel!
    
    //MARK: - Actions
    @IBAction func mapViewButton(_ sender: UIButton) {
        
    }
    
    @IBAction func placeInfoButton(_ sender: UIButton) {
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
    func placeData(cellforRowAt indexPath: IndexPath)
    {
        delegate?.placeData(at: self, cellForRowAt: indexPath)
    }
    
    func setPlace(_ place: Partner)
    {
        placeNameLabel.text = place.companyName
        placeAddressLabel.text = String(place.companyStreetNumber!) + " " + place.companyStreet! + "\n" + place.companyCity!
        placeRateLabel.text = "asd"
        placeDistanceLabel.text = "asd" //változóba kitenni a saját pozíciódtól mért távolságot castovlva
        
    }
    
}
