//
//  ListTabBarViewController.swift
//  ProjectWork_v1
//
//  Created by Levente Szabó on 2018. 11. 02..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit

class ListTabBarViewController: UIViewController, UIScrollViewDelegate {

    //MARK: Outlets
    
    @IBOutlet weak var filterViewContainer: UIView!
    @IBOutlet weak var placeListScrollView: UIScrollView!
    
    
    //MARK: Actions
    @IBAction func showFilterView(_ sender: UIBarButtonItem) {
        
        if let filterPanel = Bundle.main.loadNibNamed("FilterView", owner: nil, options: nil)![0] as? FilterView
        {
            //Feltétel, ami gátolja, hogy több subview kerüljön egymásra.
            
            filterViewContainer.addSubview(filterPanel)
                
                //Disappearing the filter view
            
        }
       
    }
    //
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        // Do any additional setup after loading the view.
//        if let filterPanel = Bundle.main.loadNibNamed("FilterView", owner: nil, options: nil)![0] as? FilterView
//        {
//                filterViewContainer.addSubview(filterPanel)
//        }
//    }
////
//    override func viewDidLayoutSubviews() {
//        if (filterViewContainer.subviews.count > 0)
//        {
//            filterViewContainer.subviews[0].frame = filterViewContainer.bounds
//        }
  }
    //MARK: - ScrollView
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
            let numberOfPlaces = 10
            for i in stride(from: 0, to: numberOfPlaces, by: 1)
            {
                //Add places to the list
                if let placeView = Bundle.main.loadNibNamed("PlaceListView", owner: nil, options: nil)![0] as? PlaceListView
                {
                    let newPlaceView = UIView()         //Nem biztos, hogy kell
                    newPlaceView.addSubview(placeView)  //Nem biztos, hogy kell
                    placeListScrollView.addSubview(newPlaceView)
                    //A saját színemet beállítani
                    //placeView.backgroundColor = UIColor.green
                    placeView.frame = CGRect(x: 0,
                                             y: 10 + (placeListScrollView.frame.height * CGFloat(i)),     //MaxY
                                             width: placeListScrollView.frame.width-20,
                                             height: placeListScrollView.frame.height-20)
                    //MARK: - View style
                    newPlaceView.layer.cornerRadius = 10
                    newPlaceView.layer.borderWidth = 5
                    //newPlaceView.layer.borderColor
                }
            }
            placeListScrollView.delegate = self
            //placeListScrollView.setContentOffset(CGPoint(x:0,y:100), animated: true)
            placeListScrollView.isPagingEnabled = true
            placeListScrollView.contentSize = CGSize(width: placeListScrollView.frame.width - 10 /*+ 10*/, height: CGFloat(numberOfPlaces) * placeListScrollView.frame.height)
        
    }
    override func viewDidLayoutSubviews() {
        
        //MARK: Filter
        if (filterViewContainer.subviews.count > 0)
        {
            filterViewContainer.subviews[0].frame = filterViewContainer.bounds
        }
        
        //MARK: ScrollView
        if (placeListScrollView.subviews.count > 0)
        {
            for subviews in placeListScrollView.subviews
            {
                subviews.frame = CGRect(x: 0,
                                        y: CGFloat(subviews.tag) * placeListScrollView.frame.height,
                                        width: placeListScrollView.frame.width,
                                        height: placeListScrollView.frame.height)
            }
            //placeListScrollView.subviews[0].frame = placeListScrollView.bounds
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK: - TableView
}
