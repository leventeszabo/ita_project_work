//
//  ViewController.swift
//  ProjectWork_v1
//
//  Created by Levente Szabó on 2018. 10. 14..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIAlertViewDelegate {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var loginAlert: UIAlertController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        
        //MARK: - Read data from csv
        readCsv(fileName: "RegisteredUserList", fileType: ".csv")
        
        var data = readCsv(fileName: "RegisteredUserList", fileType: ".csv")
//        data = cleanRows(file: data)
//        let csvRows = readUsersCsv(data: data)
       // print(csvRows[1][1])
        
    }

//    @IBAction func RegistrateNewUser(_ sender: UIButton) {
////        let newUser = User(newFirstName: <#T##String#>, newLastName: <#T##String#>, newEmailAddress: <#T##String#>, newYearOfBirth: <#T##Int#>, newMonthOfBirth: <#T##Int#>, newDayOfBirth: <#T##Int#>, newMobileNumber: <#T##String#>, newBirthday: <#T##Date#>, newGenderOfUser: <#T##String#>, newStatus: <#T##String#>)
//
//    }
    
    //MARK: read data from local csv file
    func readFromLocalCsv ()
    {
        //MARK: csv path
        
        //MARK: read from file
        
    }
    
    @IBAction func loginButton(_ sender: UIButton) {
        if(usernameTextField.text != "" && passwordTextField.text != "")
        {
            print("bejelentkezés")
        }
        else
        {
            loginAlert = UIAlertController(title: "Mistake", message: "Check your username or password.", preferredStyle: UIAlertController.Style.alert)
            loginAlert?.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(loginAlert!, animated: true, completion: nil)
        }
    //Singleton for add data to profile view
    
    }
    
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
     //MARK: - CSV import
    func readCsv(fileName: String, fileType: String)
    {
        
        //let readUserFileName = "RegisteredUserList"
        let readDocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: false)
        let readFileURL = readDocumentDirURL.appendingPathComponent(fileName/*readUserFileName*/).appendingPathExtension(fileType/*".csv"*/)
        
        do {
            let text2 = try String(contentsOf: readFileURL, encoding: .utf8)
            
            print(text2)
        }
        catch {
            print("CANT READ FILE")
            
        }
    }
    
    func cleanRows(file:String)->String{
    var cleanFile = file
    cleanFile = cleanFile.replacingOccurrences(of: "\r", with: "\n")
    cleanFile = cleanFile.replacingOccurrences(of: "\n\n", with: "\n")
    //        cleanFile = cleanFile.replacingOccurrences(of: ";;", with: "")
    //        cleanFile = cleanFile.replacingOccurrences(of: ";\n", with: "")
    return cleanFile
    }
    
    func readUsersCsv(data: String) -> [[String]]
    {
        var registeredUserArray: [[String]] = []
        let dataRows = data.components(separatedBy: "\n")
        for row in dataRows
        {
            let dataColumns = row.components(separatedBy: ";")
            registeredUserArray.append(dataColumns)
        }
        
        
        return registeredUserArray
    }
}

