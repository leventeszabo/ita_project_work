//
//  Status.swift
//  ProjectWork_v1
//
//  Created by Levente Szabó on 2018. 10. 16..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit

enum Status: Int {
    case student = 0//"Student"
    case partner = 1//"Partner company"
}
