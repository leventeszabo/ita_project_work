//
//  gender.swift
//  ProjectWork_v1
//
//  Created by Levente Szabó on 2018. 10. 15..
//  Copyright © 2018. Levente Szabó. All rights reserved.
//

import UIKit

enum Gender: Int {
    case man = 0
    case woman = 1
}
